# PRIAK

Priak stands short for Print Access Key. This is a tool meant to receive an NF-e access key and output it in a
specified format. This may aid with some automation by reducing how much awk or grep you'd need otherwise.

## Formating flags

 - u: prints the digits that encodes the UF;

 - y: prints the digits that encodes the emission year;

 - m: prints the digits that encodes the emission month;

 - c: prints the digits of the emitter's CNPJ;

 - d: prints the digits that encodes the document model;

 - s: prints the digits of the serial number;

 - n: prints the digits of the document number;

 - e: prints the digit that encodes the emission type;

 - r: prints the digit that encodes the random numeric number;

 - v: prints the validation digit.

Any characters and symbols that arent any of these are kept up as a mask. See an example
in the examples session.


## Examples

 - Printing the access key as is:
```
priak uymcdsnerv
```

 - Printing the inverted access key:
```
priak vrensdcmyu
```

 - Printing the CNPJ, Document Model, Document Number and then emission date:
```
priak cdnym
```

 - Printing the document information in a structured way (i.e to aid with sorting some data):
```
priak c: y/m => d[s:n]
```


## Coding Style and Guidelines

There are a whole lot of ways of passing parameters around in C++, and they mostly affect the possibilities in behavior
in many ways. To keep stuff simple and intuitive:

- Pass by pointer: caller is responsible to guarantee that the data refered by the pointer will be valid until the
called method/function returns. If it was a constructor, the the caller must guarantee that the data will outlive the
callee, or at least until it is made sure that the data isn't needed anymore (though this should never happen). Null must
be a valid value; references should be used otherwise;

- Pass by reference: passing by reference holds the same meanings and motivations as passing by pointer. The lifetime of
data follows the same guidelines. Passing by reference is used whenever null is not a valid parameter value. As references
cannot change to other objects after initialized (afaik), this can also pass some data lifetime intentions;