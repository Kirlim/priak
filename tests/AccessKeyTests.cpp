
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<AccessKey.h>



namespace AccessKeyTests
{
	TEST_CASE("Validating that indexes match up and have 44 characters mapped", "[Unit - AccessKey]")
	{
		REQUIRE(Priak::AccessKey::IndexStartUF == 0);
		REQUIRE(Priak::AccessKey::IndexEndUF == Priak::AccessKey::IndexStartUF + 2 - 1);

		REQUIRE(Priak::AccessKey::IndexStartYear == Priak::AccessKey::IndexEndUF + 1);
		REQUIRE(Priak::AccessKey::IndexEndYear == Priak::AccessKey::IndexStartYear + 2 - 1);

		REQUIRE(Priak::AccessKey::IndexStartMonth == Priak::AccessKey::IndexEndYear + 1);
		REQUIRE(Priak::AccessKey::IndexEndMonth == Priak::AccessKey::IndexStartMonth + 2 - 1);

		REQUIRE(Priak::AccessKey::IndexStartCNPJ == Priak::AccessKey::IndexEndMonth + 1);
		REQUIRE(Priak::AccessKey::IndexEndCNPJ == Priak::AccessKey::IndexStartCNPJ + 14 - 1);

		REQUIRE(Priak::AccessKey::IndexStartDocumentModel == Priak::AccessKey::IndexEndCNPJ + 1);
		REQUIRE(Priak::AccessKey::IndexEndDocumentModel == Priak::AccessKey::IndexStartDocumentModel + 2 - 1);

		REQUIRE(Priak::AccessKey::IndexStartSerial == Priak::AccessKey::IndexEndDocumentModel + 1);
		REQUIRE(Priak::AccessKey::IndexEndSerial == Priak::AccessKey::IndexStartSerial + 3 - 1);

		REQUIRE(Priak::AccessKey::IndexStartNumber == Priak::AccessKey::IndexEndSerial + 1);
		REQUIRE(Priak::AccessKey::IndexEndNumber == Priak::AccessKey::IndexStartNumber + 9 - 1);

		REQUIRE(Priak::AccessKey::IndexStartEmissionType == Priak::AccessKey::IndexEndNumber + 1);
		REQUIRE(Priak::AccessKey::IndexEndEmissionType == Priak::AccessKey::IndexStartEmissionType + 1 - 1);

		REQUIRE(Priak::AccessKey::IndexStartNumericCode == Priak::AccessKey::IndexEndEmissionType + 1);
		REQUIRE(Priak::AccessKey::IndexEndNumericCode == Priak::AccessKey::IndexStartNumericCode + 8 - 1);

		REQUIRE(Priak::AccessKey::IndexStartValidationDigit == Priak::AccessKey::IndexEndNumericCode + 1);
		REQUIRE(Priak::AccessKey::IndexEndValidationDigit == Priak::AccessKey::IndexEndValidationDigit + 1 - 1);
		REQUIRE(Priak::AccessKey::IndexEndValidationDigit == 43);
	}
}
