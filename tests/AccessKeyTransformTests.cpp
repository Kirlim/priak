
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<AccessKeyTransform.h>



namespace AccessKeyTransformTests
{
	TEST_CASE("Access Key Tranform follows the masks", "[Unit - AccessKeyTransform]")
	{
		SECTION("No format means no output")
		{
			Priak::AccessKeyTransform akt{ "" };
			REQUIRE(akt.Transform("12345678901234567890123456789012345678901234") == "");
		}

		SECTION("UF only")
		{
			Priak::AccessKeyTransform akt{ "u" };
			REQUIRE(akt.Transform("12000000000000000000000000000000000000000000") == "12");
		}

		SECTION("Year only")
		{
			Priak::AccessKeyTransform akt{ "y" };
			REQUIRE(akt.Transform("00190000000000000000000000000000000000000000") == "19");
		}

		SECTION("Month only")
		{
			Priak::AccessKeyTransform akt{ "m" };
			REQUIRE(akt.Transform("00001100000000000000000000000000000000000000") == "11");
		}

		SECTION("CNPJ only")
		{
			Priak::AccessKeyTransform akt{ "c" };
			REQUIRE(akt.Transform("00000012345678901234000000000000000000000000") == "12345678901234");
		}

		SECTION("Document Model only")
		{
			Priak::AccessKeyTransform akt{ "d" };
			REQUIRE(akt.Transform("00000000000000000000670000000000000000000000") == "67");
		}

		SECTION("Serial Number only")
		{
			Priak::AccessKeyTransform akt{ "s" };
			REQUIRE(akt.Transform("00000000000000000000001230000000000000000000") == "123");
		}

		SECTION("Document Number only")
		{
			Priak::AccessKeyTransform akt{ "n" };
			REQUIRE(akt.Transform("00000000000000000000000001234567890000000000") == "123456789");
		}

		SECTION("Emission Type only")
		{
			Priak::AccessKeyTransform akt{ "e" };
			REQUIRE(akt.Transform("00000000000000000000000000000000001000000000") == "1");
		}

		SECTION("Random Number only")
		{
			Priak::AccessKeyTransform akt{ "r" };
			REQUIRE(akt.Transform("00000000000000000000000000000000000123456780") == "12345678");
		}

		SECTION("Validation Digit only")
		{
			Priak::AccessKeyTransform akt{ "v" };
			REQUIRE(akt.Transform("00000000000000000000000000000000000000000001") == "1");
		}

		SECTION("Inverting the access key")
		{
			Priak::AccessKeyTransform akt{ "vrensdcmyu" };
			REQUIRE(akt.Transform("11223344444444444444556667777777778999999990") == "09999999987777777776665544444444444444332211");
		}

		SECTION("Masking with anything that is not a character")
		{
			Priak::AccessKeyTransform akt{ "c: y/m => d[s:n]" };
			REQUIRE(akt.Transform("11223344444444444444556667777777778999999990") == "44444444444444: 22/33 => 55[666:777777777]");
		}
	}
}
