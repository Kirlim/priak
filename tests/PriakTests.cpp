
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Priak.h>
#include<AccessKeyTransform.h>

#include"Implementations/MockedAccessKeyReader.h"
#include"Implementations/MockedOutputWriter.h"



namespace PriakTests
{
	SCENARIO("Transforming access key with input", "[Acceptance - Priak]")
	{
		GIVEN("I have two access keys to transform")
		{
			MockedAccessKeyReader reader{
				"12345678901234567890123456789012345678901234",
				"11223344444444444444556667777777778999999990"
			};
			MockedOutputWriter writer{};

			WHEN("I run Priak with an empty mask")
			{
				Priak::AccessKeyTransform transformer{ "" };
				Priak::Priak priak{ reader, writer, transformer };
				priak.RunToCompletion();

				THEN("I'll receive one empty output for each access key")
				{
					REQUIRE(writer.GetOutputsCount() == 2);
					REQUIRE(writer.GetOutputAtIndex(0) == "");
					REQUIRE(writer.GetOutputAtIndex(1) == "");
				}
			}

			WHEN("I run Priak with a mask without transformation symbols")
			{
				Priak::AccessKeyTransform transformer{ "=>?,.;" };
				Priak::Priak priak{ reader, writer, transformer };
				priak.RunToCompletion();

				THEN("I'll receive the exact mask for each access key")
				{
					REQUIRE(writer.GetOutputsCount() == 2);
					REQUIRE(writer.GetOutputAtIndex(0) == "=>?,.;");
					REQUIRE(writer.GetOutputAtIndex(1) == "=>?,.;");
				}
			}

			WHEN("I run Priak with a mask to invert an access key information")
			{
				Priak::AccessKeyTransform transformer{ "vrensdcmyu" };
				Priak::Priak priak{ reader, writer, transformer };
				priak.RunToCompletion();

				THEN("I'll receive the access keys with the information inverted")
				{
					REQUIRE(writer.GetOutputsCount() == 2);
					REQUIRE(writer.GetOutputAtIndex(0) == "46789012356789012343451278901234567890563412");
					REQUIRE(writer.GetOutputAtIndex(1) == "09999999987777777776665544444444444444332211");
				}
			}

			WHEN("I run Priak to return the cnpj and emission information with a given readable mask")
			{
				Priak::AccessKeyTransform transformer{ "c: y/m => d[s:n]" };
				Priak::Priak priak{ reader, writer, transformer };
				priak.RunToCompletion();

				THEN("I'll receive the formatted information of each access key")
				{
					REQUIRE(writer.GetOutputsCount() == 2);
					REQUIRE(writer.GetOutputAtIndex(0) == "78901234567890: 34/56 => 12[345:678901234]");
					REQUIRE(writer.GetOutputAtIndex(1) == "44444444444444: 22/33 => 55[666:777777777]");
				}
			}
		}
	}
}
