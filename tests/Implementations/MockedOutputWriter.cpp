
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"MockedOutputWriter.h"



void MockedOutputWriter::Write(const char* output)
{
	m_Outputs.emplace_back(output);
}



int MockedOutputWriter::GetOutputsCount() const
{
	return int(m_Outputs.size());
}



std::string MockedOutputWriter::GetOutputAtIndex(int index) const
{
	return m_Outputs[index];
}
