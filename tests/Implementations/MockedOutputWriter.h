
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<Interfaces/OutputWriter.h>

#include<string>
#include<vector>



class MockedOutputWriter : public Priak::Interfaces::OutputWriter
{
public:
	void Write(const char* output) override;

	int GetOutputsCount() const;
	std::string GetOutputAtIndex(int index) const;



private:
	std::vector<std::string> m_Outputs;
};
