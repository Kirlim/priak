
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"MockedAccessKeyReader.h"



MockedAccessKeyReader::MockedAccessKeyReader(std::initializer_list<std::string> inputLines) :
	storedInputLines(inputLines)
{
}



std::string MockedAccessKeyReader::ReadNextAccessKey()
{
	if (nextLine < storedInputLines.size())
	{
		return storedInputLines[nextLine++];
	}

	return std::string{};
}
