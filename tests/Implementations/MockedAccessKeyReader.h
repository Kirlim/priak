
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<initializer_list>
#include<vector>

#include<Interfaces/AccessKeyReader.h>



class MockedAccessKeyReader : public Priak::Interfaces::AccessKeyReader
{
public:
	MockedAccessKeyReader(std::initializer_list<std::string> inputLines);
	std::string ReadNextAccessKey() override;

	std::vector<std::string> storedInputLines;
	int nextLine = 0;
};
