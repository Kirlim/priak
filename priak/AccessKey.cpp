
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.
//

#include"AccessKey.h"



namespace Priak
{
	namespace AccessKey
	{
		const int IndexStartUF = 0, IndexEndUF = 1;
		const int IndexStartYear = 2, IndexEndYear = 3;
		const int IndexStartMonth = 4, IndexEndMonth = 5;
		const int IndexStartCNPJ = 6, IndexEndCNPJ = 19;
		const int IndexStartDocumentModel = 20, IndexEndDocumentModel = 21;
		const int IndexStartSerial = 22, IndexEndSerial = 24;
		const int IndexStartNumber = 25, IndexEndNumber = 33;
		const int IndexStartEmissionType = 34, IndexEndEmissionType = 34;
		const int IndexStartNumericCode = 35, IndexEndNumericCode = 42;
		const int IndexStartValidationDigit = 43, IndexEndValidationDigit = 43;
	}
}
