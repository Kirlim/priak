
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>



namespace Priak
{
	namespace Interfaces
	{
		class AccessKeyReader
		{
		public:
			virtual ~AccessKeyReader() {}
			virtual std::string ReadNextAccessKey() = 0;
		};
	}
}
