
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Priak
{
	namespace Interfaces
	{
		class OutputWriter
		{
		public:
			virtual ~OutputWriter() {}
			virtual void Write(const char* output) = 0;
		};
	}
}