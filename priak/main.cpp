
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.
//

#include<iostream>
#include<string>

#include"Implementations/StdinAccessKeyReader.h"
#include"Implementations/StdoutOutputWriter.h"
#include"AccessKeyTransform.h"
#include"Priak.h"



void PrintHelp();
std::string BuildMaskParameter(int argc, char* argv[]);



int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		PrintHelp();
		return 0;
	}

	std::string parameters = argv[1];
	if (parameters == "-h")
	{
		PrintHelp();
		return 0;
	}

	parameters = BuildMaskParameter(argc, argv);
	Priak::StdinAccessKeyReader reader{};
	Priak::StdoutOutputWriter writer{};
	Priak::AccessKeyTransform transformer{ parameters.c_str() };

	Priak::Priak priak{ reader, writer, transformer };
	priak.RunToCompletion();

	return 0;
}



void PrintHelp()
{
	std::cout << "USAGE: priak [action] " << std::endl;
	std::cout << "Actions: " << std::endl;
	std::cout << "\t-h\tShows this text" << std::endl;
	std::cout << "\t(mask)\tWill output all read access keys by following the given mask" << std::endl;
	std::cout << std::endl;
	std::cout << "Mask parameters:" << std::endl;
	std::cout << "\t- u: prints the digits that encodes the UF" << std::endl;
	std::cout << "\t- y: prints the digits that encodes the emission year" << std::endl;
	std::cout << "\t- m: prints the digits that encodes the emission month" << std::endl;
	std::cout << "\t- c: prints the digits of the emitter's CNPJ" << std::endl;
	std::cout << "\t- d: prints the digits that encodes the document model" << std::endl;
	std::cout << "\t- s: prints the digits of the serial number" << std::endl;
	std::cout << "\t- n: prints the digits of the document number" << std::endl;
	std::cout << "\t- e: prints the digit that encodes the emission type" << std::endl;
	std::cout << "\t- r: prints the digit that encodes the random numeric number" << std::endl;
	std::cout << "\t- v: prints the validation digit" << std::endl;
	std::cout << "\n\tAny symbols that does not have special meaning, are considered as text of the mask." << std::endl;
}



std::string BuildMaskParameter(int argc, char* argv[])
{
	std::string parameters{};

	for (int i = 1; i < argc; i++)
	{
		parameters += " ";
		parameters += argv[i];
	}

	return parameters;
}
