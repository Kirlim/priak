
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Priak.h"

#include<string>

#include"Interfaces/AccessKeyReader.h"
#include"Interfaces/OutputWriter.h"
#include"AccessKeyTransform.h"



namespace Priak
{
	Priak::Priak(
		Interfaces::AccessKeyReader& accessKeyReader,
		Interfaces::OutputWriter& outputWriter,
		const AccessKeyTransform& accessKeyTransformer
	) :
		m_AccessKeyReader(accessKeyReader),
		m_OutputWriter(outputWriter),
		m_AccessKeyTransformer(accessKeyTransformer)
	{
	}



	void Priak::RunToCompletion() const
	{
		std::string input = m_AccessKeyReader.ReadNextAccessKey();
		while (input != "")
		{
			auto transformed = m_AccessKeyTransformer.Transform(input);
			m_OutputWriter.Write(transformed.c_str());
			input = m_AccessKeyReader.ReadNextAccessKey();
		}
	}
}