
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Priak
{
	namespace Interfaces
	{
		class AccessKeyReader;
		class OutputWriter;
	}

	class AccessKeyTransform;



	// Priak is the main class that encapsulates the access key reading, transforming and writing
	// for orchestration.
	class Priak
	{
	public:
		Priak(
			Interfaces::AccessKeyReader& accessKeyReader,
			Interfaces::OutputWriter& outputWriter,
			const AccessKeyTransform& accessKeyTransformer
		);

		// Reads from input and output the desired data
		// until the input returns an empty string.
		void RunToCompletion() const;



	private:
		Interfaces::AccessKeyReader& m_AccessKeyReader;
		Interfaces::OutputWriter& m_OutputWriter;
		const AccessKeyTransform& m_AccessKeyTransformer;
	};
}