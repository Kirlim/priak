
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<vector>



namespace Priak
{
	enum class AccessKeyTransformation
	{
		PrintUF,
		PrintYear,
		PrintMonth,
		PrintCNPJ,
		PrintDocumentModel,
		PrintSerial,
		PrintNumber,
		PrintEmissionType,
		PrintNumericCode,
		PrintValidationDigit,

		PrintFormatStringSegment
	};



	class AccessKeyTransform
	{
	public:
		AccessKeyTransform(const char* formatString);

		std::string Transform(std::string accessKey) const;



	private:
		void BuildPrintCommands();



	private:
		const char* m_FormatString;

		struct PrintCommand
		{
			bool fromFormatString;
			int substrPositionStart;
			int substrPositionEnd;

			PrintCommand(bool fromFormatString, int substrPositionStart, int substrPositionEnd);
		};

		std::vector<PrintCommand> m_PrintCommands;
	};
}