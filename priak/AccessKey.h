
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<cstdint>



namespace Priak
{
	namespace AccessKey
	{
		extern const int IndexStartUF, IndexEndUF;
		extern const int IndexStartYear, IndexEndYear;
		extern const int IndexStartMonth, IndexEndMonth;
		extern const int IndexStartCNPJ, IndexEndCNPJ;
		extern const int IndexStartDocumentModel, IndexEndDocumentModel;
		extern const int IndexStartSerial, IndexEndSerial;
		extern const int IndexStartNumber, IndexEndNumber;
		extern const int IndexStartEmissionType, IndexEndEmissionType;
		extern const int IndexStartNumericCode, IndexEndNumericCode;
		extern const int IndexStartValidationDigit, IndexEndValidationDigit;
	};
}
