
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"StdoutOutputWriter.h"

#include<iostream>



namespace Priak
{
	void StdoutOutputWriter::Write(const char* output)
	{
		std::cout << output << std::endl;
	}
}
