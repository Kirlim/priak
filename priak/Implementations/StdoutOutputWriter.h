
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"../Interfaces/OutputWriter.h"



namespace Priak
{
	// The StdoutOutputWriter class will output the given lines to the
	// standard output.
	class StdoutOutputWriter : public Interfaces::OutputWriter
	{
	public:
		void Write(const char* output) override;
	};
}
