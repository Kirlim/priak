
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.
//

#include"StdinAccessKeyReader.h"

#include<iostream>



namespace Priak
{
	std::string StdinAccessKeyReader::ReadNextAccessKey()
	{
		std::string input{};
		std::cin >> input;
		return input;
	}
}
