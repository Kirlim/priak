
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"AccessKeyTransform.h"

#include<cstring>

#include"AccessKey.h"



namespace Priak
{
	AccessKeyTransform::AccessKeyTransform(const char* formatString) :
		m_FormatString(formatString)
	{
		BuildPrintCommands();
	}



	std::string AccessKeyTransform::Transform(std::string accessKey) const
	{
		std::string result{};

		for (const auto& printCommand : m_PrintCommands)
		{
			if (printCommand.fromFormatString)
			{
				// With the current implementation, only one character is needed
				result += m_FormatString[printCommand.substrPositionEnd];
			}
			else
			{
				result += accessKey.substr(
					printCommand.substrPositionStart,
					printCommand.substrPositionEnd - printCommand.substrPositionStart + 1
				);
			}
		}

		return result;
	}



	void AccessKeyTransform::BuildPrintCommands()
	{
		for (int i = 0; i < strlen(m_FormatString); ++i)
		{
			switch (m_FormatString[i])
			{
			case 'u':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartUF, AccessKey::IndexEndUF);
				break;
			case 'y':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartYear, AccessKey::IndexEndYear);
				break;
			case 'm':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartMonth, AccessKey::IndexEndMonth);
				break;
			case 'c':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartCNPJ, AccessKey::IndexEndCNPJ);
				break;
			case 'd':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartDocumentModel, AccessKey::IndexEndDocumentModel);
				break;
			case 's':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartSerial, AccessKey::IndexEndSerial);
				break;
			case 'n':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartNumber, AccessKey::IndexEndNumber);
				break;
			case 'e':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartEmissionType, AccessKey::IndexEndEmissionType);
				break;
			case 'r':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartNumericCode, AccessKey::IndexEndNumericCode);
				break;
			case 'v':
				m_PrintCommands.emplace_back(false, AccessKey::IndexStartValidationDigit, AccessKey::IndexEndValidationDigit);
				break;
			default:
				// TODO : this can be optimized
				m_PrintCommands.emplace_back(true, i, i);
				break;
			}
		}
	}



	AccessKeyTransform::PrintCommand::PrintCommand(bool fromFormatString, int substrPositionStart, int substrPositionEnd) :
		fromFormatString(fromFormatString), substrPositionStart(substrPositionStart), substrPositionEnd(substrPositionEnd)
	{
	}
}
